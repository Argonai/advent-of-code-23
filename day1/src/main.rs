use std::{fs::File, io::{BufReader, BufRead}, collections::HashMap};
use regex::{self, Regex};

fn main() {
    let word_map = HashMap::from([
        ("one", 1),
        ("two", 2),
        ("three", 3),
        ("four", 4),
        ("five", 5),
        ("six", 6),
        ("seven", 7),
        ("eight", 8),
        ("nine", 9),
    ]);
    let reg = "one|two|three|four|five|six|seven|eight|nine";
    let re = Regex::new(format!("{}{}{}","(" , reg , "|\\d)").as_str()).expect("msg");
    let revers = Regex::new(format!("{}{}{}","(", reg.chars().rev().collect::<String>(), "|\\d)").as_str()).expect("msg");
    let file = File::open("./src/input.txt").expect("msg");
    let reader = BufReader::new(file);
    let mut total = 0;
    for line in reader.lines() {
        let templine = line.expect("msg");
            total += format!("{}{}", mapper(search(&templine, &re), &word_map),
             mapper(reverse(search(&templine.chars().rev().collect::<String>(), &revers)).as_str(), &word_map)).parse::<i32>().expect("msg");
    }
    print!("{}\n", total)
}
//searchleft en searchright functie
fn search<'a>(line: &'a String, re: &'a Regex) -> &'a str{
    let temp: Vec<_> = re.find_iter(&line).map(|m| m.as_str()).collect();
    let first = temp.first().expect("msg").to_owned();
    first
}
fn reverse(string: &str) -> String{
    string.chars().rev().collect::<String>()
}
fn mapper(string: &str, word_map: &HashMap<&str, i32>) ->i32 {
    
    if string.parse::<i32>().is_ok() {
        string.parse::<i32>().expect("msg")
    }else {
        word_map[string]
    }
}